const Sequelize = require("sequelize");
const fs = require("fs");
const { promisify } = require("util");
const deleteFile = promisify(fs.unlink);

const sequelize = require("../util/database");

const Image = sequelize.define(
  "image",
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },
    path: {
      type: Sequelize.STRING,
    },
    isFavorite: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    originalName: Sequelize.STRING,
  },
  {
    hooks: {
      beforeDestroy: (instance, options) => {
        return deleteFile(instance.path);
      },
    },
  }
);

module.exports = Image;
