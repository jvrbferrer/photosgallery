const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const session = require("express-session");
const csrf = require("csurf");
const flash = require("connect-flash");
const SequelizeStore = require("connect-session-sequelize")(session.Store);
const multer = require("multer");
const crypto = require("crypto");
if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

const Gallery = require("./models/gallery");
const Image = require("./models/image");

const errorController = require("./controllers/error");
const sequelize = require("./util/database");
const routes = require("./routes/gallery");
const adminRoutes = require("./routes/admin");

const app = express();
const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "photos");
  },
  filename: (req, file, cb) => {
    crypto.randomBytes(16, (err, buffer) => {
      if (err) {
        console.log(err);
        return res.sendStatus(500);
      }
      const token = buffer.toString("hex");
      cb(null, token + "-" + file.originalname);
    });
  },
});
const fileFilter = (req, file, cb) => {
  if (
    file.mimetype == "image/png" ||
    file.mimetype == "image/jpg" ||
    file.mimetype == "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
    return cb(new Error("Only .png, .jpg and .jpeg format allowed!"));
  }
};
const store = new SequelizeStore({
  db: sequelize,
});
const csrfProtection = csrf();
app.set("view engine", "ejs");
app.set("views", "views");

// app.use(require("morgan")("combined"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(
  multer({
    storage: fileStorage,
    fileFilter,
  }).fields([
    { name: "photos" },
    { name: "galleryImage", maxCount: 1 },
    { name: "file" },
  ])
);

app.use(express.static(path.join(__dirname, "public")));
app.use("/photos", express.static(path.join(__dirname, "photos")));
app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    store,
  })
);
app.use(csrfProtection);
app.use(flash());
app.use((req, res, next) => {
  res.locals.csrfToken = req.csrfToken();
  next();
});

app.use("/admin", adminRoutes);
app.use("/", routes);
app.use(errorController.get404);

Image.Gallery = Image.belongsTo(Gallery);
Gallery.Images = Gallery.hasMany(Image);

Gallery.GalleryImage = Gallery.belongsTo(Image, {
  as: "galleryImage",
  constraints: false,
});

sequelize
  .sync()
  .then(() => store.sync())
  .then((result) => {
    app.listen(process.env.PORT || 3000, () => {
      console.log("App started");
    });
  })
  .catch((err) => {
    console.log(err);
  });

// Run docker mysql image
// docker run -v ./db:/var/lib/mysql -p 3306:3306 -e MYSQL_DATABASE=db_dev -e MYSQL_ROOT_PASSWORD=my-secret-pw mysql:latest
