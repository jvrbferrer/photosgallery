let photos = null;
let i = null;
let galleryPhoto = null;
let downloadTag = null;
let favoriteIconImage = null;

const openGalleryModal = (imageClicked) => {
  galleryPhoto = document.getElementById("modal-img");
  downloadTag = document.getElementById("download-photo-link");
  favoriteIconImage = document.getElementsByClassName(
    "gallery-modal__header__utils__icon"
  )[0];

  galleryPhoto.src = imageClicked.src;
  downloadTag.href = imageClicked.src;
  downloadTag.download = imageClicked.getAttribute("data-originalName");

  document.getElementById("gallery-modal").style.display = "flex";
  //disable scroll
  document.getElementsByTagName("body")[0].style.overflow = "hidden";
  document.getElementsByTagName("html")[0].style.overflow = "hidden";

  photos = document.getElementsByClassName("photo-card__photo");
  i = [...photos].indexOf(imageClicked);
  updateFavoriteIcon();
};

const closeGalleryModal = () => {
  document.getElementById("gallery-modal").style.display = "none";

  //enable scoll
  document.getElementsByTagName("body")[0].style.overflow = "auto";
  document.getElementsByTagName("html")[0].style.overflow = "auto";
};

const selectNextPhoto = () => {
  i = (i + 1) % photos.length;
  galleryPhoto.src = photos[i].src;
  downloadTag.href = photos[i].src;
  downloadTag.download = photos[i].getAttribute("data-originalName");
  updateFavoriteIcon();
};

const selectPreviousPhoto = () => {
  i = (i - 1 + photos.length) % photos.length;
  galleryPhoto.src = photos[i].src;
  downloadTag.href = photos[i].src;
  downloadTag.download = photos[i].getAttribute("data-originalName");
  updateFavoriteIcon();
};

const toggleFavorite = () => {
  fetch(`/gallery/setFavoritePhoto/${photos[i].id}`, {
    method: "POST",
    headers: {
      "csrf-token": document.querySelector('meta[name="csrf-token"]').content,
    },
  })
    .then((res) => {
      if (res.ok) {
        photos[i].setAttribute(
          "data-isFavorite",
          photos[i].getAttribute("data-isFavorite") === "true"
            ? "false"
            : "true"
        );
        updateFavoriteIcon();
      } else {
        console.log(res);
      }
    })
    .catch((err) => console.log(err));
};

const updateFavoriteIcon = () => {
  if (photos[i].getAttribute("data-isFavorite") === "true") {
    favoriteIconImage.src = "/images/favorite_icon.svg";
  } else {
    favoriteIconImage.src = "/images/unfavorite_icon.svg";
  }
};
