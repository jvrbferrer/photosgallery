const Image = require("../models/image");

exports.verifyAdmin = (req, res, next) => {
  if (!req.session.admin) {
    return res.redirect("/admin/login");
  }
  next();
};

exports.buildVerifyGalleryLoginParam = ({ param, body, fromPhoto }) => {
  return async (req, res, next) => {
    galleryId = null;
    if (fromPhoto) {
      try {
        const image = await Image.findByPk(
          param ? req.params[param] : req.body[body]
        );
        galleryId = image.galleryId.toString();
      } catch (err) {
        console.log(err);
        return res.sendStatus(500);
      }
    } else if (param) {
      galleryId = req.params[param];
    } else {
      galleryId = req.body[body];
    }
    if (req.session.authGalleryId === galleryId) {
      return next();
    }
    return res.redirect(`/gallery/login/${galleryId}`);
  };
};
