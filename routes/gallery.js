const express = require("express");
const galleryController = require("../controllers/gallery");
const { buildVerifyGalleryLoginParam } = require("../middleware/auth");

const router = express.Router();

router.get("/", galleryController.getIndex);
router.get("/gallery/login/:id", galleryController.getLogin);
router.post("/gallery/login/:id", galleryController.postLogin);
router.get(
  "/gallery/:id",
  buildVerifyGalleryLoginParam({ param: "id" }),
  galleryController.getGallery
);

router.post(
  "/gallery/setFavoritePhoto/:photoId",
  buildVerifyGalleryLoginParam({ param: "photoId", fromPhoto: true }),
  galleryController.toggleFavoritePhoto
);

router.get(
  "/gallery/:id/favorites",
  buildVerifyGalleryLoginParam({ param: "id" }),
  galleryController.getGalleryFavorites
);

router.get(
  "/gallery/:id/download",
  buildVerifyGalleryLoginParam({ param: "id" }),
  galleryController.downloadGallery
);

router.get(
  "/gallery/:id/favorites/download",
  buildVerifyGalleryLoginParam({ param: "id" }),
  galleryController.downloadGalleryFavorites
);

module.exports = router;
