const express = require("express");

const adminController = require("../controllers/admin");
const { verifyAdmin } = require("../middleware/auth");

const router = express.Router();

router.get("/login", adminController.getLogin);
router.post("/login", adminController.postLogin);

router.get("/", adminController.getIndex);

router.get("/new-gallery", verifyAdmin, adminController.getNewGallery);
router.post("/new-gallery", verifyAdmin, adminController.postNewGallery);

router.get("/edit-gallery/:id", verifyAdmin, adminController.getEditGallery);
router.post("/edit-gallery/:id", verifyAdmin, adminController.postEditGallery);

router.post("/add-photo/:id", verifyAdmin, adminController.postAddPhoto);

router.post("/delete-photo/:id", verifyAdmin, adminController.deletePhoto);
router.post("/delete-gallery/:id", verifyAdmin, adminController.deleteGallery);

module.exports = router;
