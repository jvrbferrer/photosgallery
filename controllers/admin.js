const Image = require("../models/image");
const Gallery = require("../models/gallery");
const fs = require("fs");
const { promisify } = require("util");
const deleteFile = promisify(fs.unlink);
const multer = require("multer");

const GALLERIES_PER_PAGE = 18;

exports.getIndex = async (req, res, next) => {
  if (!req.session.admin) {
    return res.redirect("/admin/login");
  }
  const offset = req.query.offset ? req.query.offset : 0;
  try {
    const { rows: galleries, count } = await Gallery.findAndCountAll({
      include: { model: Image, as: "galleryImage" },
      limit: GALLERIES_PER_PAGE,
      order: [["id", "DESC"]],
      offset: parseInt(offset),
    });
    return res.render("adminIndex", {
      pageTitle: "Admin",
      galleries,
      count,
      GALLERIES_PER_PAGE,
      page: offset / GALLERIES_PER_PAGE + 1,
    });
  } catch (error) {
    console.log(error);
    return res.status(500).send("Some error occured.");
  }
};

exports.getLogin = (req, res, next) => {
  res.render("adminLogin", { pageTitle: "Admin login" });
};

exports.postLogin = (req, res, next) => {
  if (req.body.password === process.env.ADMIN_PASSWORD) {
    req.session.admin = true;
    req.session.save((err) => {
      if (err) console.log(err);
      res.redirect("/admin");
    });
  } else {
    res.redirect("/admin");
  }
};

exports.getNewGallery = (req, res, next) => {
  res.render("newGallery", { pageTitle: "Create gallery" });
};

exports.postNewGallery = async (req, res, next) => {
  const name = req.body.name;
  const password = req.body.password;
  const expirationDate = req.body.expirationDate;
  try {
    const galleryImage = req.files["galleryImage"][0];
    await Gallery.create(
      {
        name,
        password,
        expirationDate,
        galleryImage: {
          path: galleryImage.path,
        },
      },
      {
        include: [Gallery.GalleryImage],
      }
    );
    return res.redirect("/admin");
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .send("Internal server error. Verify if you passed all the fields");
  }
};

exports.getEditGallery = (req, res, next) => {
  const galleryId = req.params.id;
  Gallery.findByPk(galleryId, {
    include: [Gallery.GalleryImage, Gallery.Images],
  })
    .then((gallery) => {
      res.render("editGallery", {
        pageTitle: "Edit Gallery",
        gallery: gallery,
      });
    })
    .catch((err) => {
      console.log(err);
      return res.sendStatus(500);
    });
};

exports.postEditGallery = async (req, res, next) => {
  const name = req.body.name;
  const password = req.body.password;
  const expirationDate = req.body.expirationDate;
  const newPhotos = req.files.photos;
  const galleryImage = req.files.galleryImage
    ? req.files.galleryImage[0]
    : null;
  const galleryId = req.params.id;

  try {
    const gallery = await Gallery.findByPk(galleryId, {
      include: Gallery.GalleryImage,
    });
    if (galleryImage) {
      await gallery.galleryImage.destroy();
      const newGalleryImage = await Image.create({
        path: galleryImage.path,
      });
      gallery.galleryImageId = newGalleryImage.id;
    }
    if (newPhotos) {
      await Image.bulkCreate(
        newPhotos.map((p) => ({ path: p.path, galleryId }))
      );
    }
    gallery.name = name;
    gallery.password = password;
    gallery.expirationDate = expirationDate;
    await gallery.save();
    return res.redirect(`/admin/edit-gallery/${galleryId}`);
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

exports.deletePhoto = (req, res, next) => {
  const id = req.params.id;
  const galleryId = req.body.galleryId;
  Image.destroy({ where: { id }, individualHooks: true })
    .then(() => {
      res.redirect(`/admin/edit-gallery/${galleryId}`);
    })
    .catch((err) => {
      console.log(err);
      return res.sendStatus(500);
    });
};

exports.deleteGallery = async (req, res, next) => {
  const galleryId = req.params.id;
  try {
    const gallery = await Gallery.findByPk(galleryId, {
      include: [Gallery.GalleryImage],
    });
    if (gallery.galleryImage) {
      await gallery.galleryImage.destroy();
    }
    await Image.destroy({
      where: { galleryId: galleryId },
      individualHooks: true,
    });
    await gallery.destroy();
    return res.redirect("/admin");
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

exports.postAddPhoto = async (req, res, next) => {
  const galleryId = req.params.id;
  const newImage = req.files.file[0];
  try {
    await Image.create({
      path: newImage.path,
      originalName: newImage.originalname ? newImage.originalname : null,
      galleryId,
    });
    res.sendStatus(200);
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};
