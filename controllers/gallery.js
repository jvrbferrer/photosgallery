const Image = require("../models/image");
const Gallery = require("../models/gallery");

const AdmZip = require("adm-zip");
const { Op } = require("sequelize");

const GALLERIES_PER_PAGE = 18;

exports.getIndex = async (req, res, next) => {
  try {
    const offset = req.query.offset ? req.query.offset : 0;
    const { rows: galleries, count } = await Gallery.findAndCountAll({
      where: {
        expirationDate: {
          [Op.gt]: new Date().toISOString(),
        },
      },
      limit: GALLERIES_PER_PAGE,
      order: [["id", "DESC"]],
      offset: parseInt(offset),
      include: { model: Image, as: "galleryImage" },
    });
    res.render("index", {
      pageTitle: "Galeria Clientes",
      galleries,
      count,
      GALLERIES_PER_PAGE,
      page: offset / GALLERIES_PER_PAGE + 1,
    });
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

exports.getGallery = async (req, res, next) => {
  try {
    const galleryId = req.params.id;
    const gallery = await Gallery.findByPk(galleryId, {
      include: [Gallery.GalleryImage, Gallery.Images],
    });
    return res.render("gallery", {
      favoritesOnly: false,
      pageTitle: gallery.name,
      gallery,
    });
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

exports.toggleFavoritePhoto = async (req, res, next) => {
  const photoId = req.params.photoId;
  const galleryId = req.params.galleryId;
  try {
    let image = await Image.findByPk(photoId);
    image.isFavorite = !image.isFavorite;
    await image.save();
    res.sendStatus(200);
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

exports.getGalleryFavorites = async (req, res, next) => {
  const galleryId = req.params.id;
  try {
    const gallery = await Gallery.findByPk(galleryId, {
      include: Gallery.GalleryImage,
    });
    const trueImages = await Image.findAll({
      where: {
        galleryId,
        isFavorite: true,
      },
    });
    gallery.images = trueImages;
    return res.render("gallery", {
      favoritesOnly: true,
      pageTitle: gallery.name,
      gallery,
    });
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

exports.downloadGallery = async (req, res, next) => {
  const galleryId = req.params.id;
  try {
    const images = await Image.findAll({
      where: {
        galleryId,
      },
    });
    console.log(images.length);
    const zip = new AdmZip();
    images.forEach((image) => {
      zip.addLocalFile(
        image.path,
        false,
        image.originalName ? image.originalName : image.path
      );
    });

    res.set("Content-Type", "application/zip");
    res.send(zip.toBuffer());
  } catch (err) {
    console.log(err);
    return res.sendStatus(500);
  }
};

exports.downloadGalleryFavorites = async (req, res, next) => {
  const galleryId = req.params.id;
  try {
    const images = await Image.findAll({
      where: {
        galleryId,
        isFavorite: true,
      },
    });
    const zip = new AdmZip();
    images.forEach((image) => {
      zip.addLocalFile(
        image.path,
        false,
        image.originalName ? image.originalName : image.path
      );
    });

    res.set("Content-Type", "application/zip");
    res.send(zip.toBuffer());
  } catch (err) {
    console.log(err);
    return res.sendStatus(500);
  }
};

exports.getLogin = async (req, res, next) => {
  try {
    const galleryId = req.params.id;
    if (req.session.authGalleryId === galleryId) {
      return res.redirect(`/gallery/${galleryId}`);
    }
    const gallery = await Gallery.findByPk(galleryId, {
      include: Gallery.GalleryImage,
    });

    return res.render("galleryAuth", { pageTitle: "Login", gallery });
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

exports.postLogin = async (req, res, next) => {
  try {
    const galleryId = req.params.id;
    const password = req.body.password;
    const gallery = await Gallery.findByPk(galleryId);
    if (gallery.password === password) {
      req.session.authGalleryId = galleryId;
      return req.session.save((err) => {
        if (err) console.log(err);
        res.redirect(`/gallery/${galleryId}`);
      });
    }
    res.redirect(`/gallery/login/${galleryId}`);
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};
